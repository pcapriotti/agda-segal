#!/bin/bash -e

function err() {
    printf "\033[0;31m$1\033[0m\n"
    exit 1
}

function log() {
    printf "\033[0;32m$1\033[0m\n"
}

[ -z "$(git status -uno --porcelain)" ] || err "Commit local changes first"
sha=$(git log -1 HEAD --pretty=format:%H | cut -c-12)

log "Fetching page repository..."
rm -fr .pages
git clone -b pages . .pages
rm -fr .pages/public

log "Generating pages..."
agda -i . --html --html-dir .pages/public README.agda

log "Deploying new pages..."
cd .pages
git add -A
if ! git commit -m "Update: $sha" 2>/dev/null; then
    log "No update needed"
fi
git push origin pages
cd -
git push origin pages

log "All done."
