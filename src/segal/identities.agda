{-# OPTIONS --without-K #-}
module segal.identities where

open import segal.identities.l1 public
open import segal.identities.l2 public
open import segal.identities.l3 public
