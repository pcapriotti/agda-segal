{-# OPTIONS --without-K #-}
module segal.composition where

open import segal.composition.l1 public
open import segal.composition.l2 public
open import segal.composition.l3 public
open import segal.composition.l4 public
