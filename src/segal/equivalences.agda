{-# OPTIONS --without-K #-}
module segal.equivalences where

open import function.core
open import function.isomorphism
open import level
open import sum

open import segal.composition
open import segal.identities

-- Summary of the equivalences proved in this development

SegalDeg₂ : (i : Level) → Set (lsuc i)
SegalDeg₂ i = Σ (Segal₂ i) (HasDeg₁ ∘ proj₁)

WildId₂ : (i : Level) → Set (lsuc i)
WildId₂ i = Σ (Wild₂ i) (HasId₁ ∘ proj₁)

segal-deg-wild-id₂ : ∀ {i} → SegalDeg₂ i ≅ WildId₂ i
segal-deg-wild-id₂ = Σ-ap-iso' segal-wild₂ λ _ → deg-id₁

SegalDeg₃ : (i : Level) → Set (lsuc i)
SegalDeg₃ i = Σ (Segal₃ i) (HasDeg₂ ∘ proj₁)

WildId₃ : (i : Level) → Set (lsuc i)
WildId₃ i = Σ (Wild₃ i) (HasId₂ ∘ proj₁)

segal-deg-wild-id₃ : ∀ {i} → SegalDeg₃ i ≅ WildId₃ i
segal-deg-wild-id₃ = Σ-ap-iso' segal-wild₃ λ _ → deg-id₂

SegalDeg₄ : (i : Level) → Set (lsuc i)
SegalDeg₄ i = Σ (Segal₄ i) (HasDeg₃ ∘ proj₁)

WildId₄ : (i : Level) → Set (lsuc i)
WildId₄ i = Σ (Wild₄ i) (HasId₃ ∘ proj₁)

segal-deg-wild-id₄ : ∀ {i} → SegalDeg₄ i ≅ WildId₄ i
segal-deg-wild-id₄ = Σ-ap-iso' segal-wild₄ λ _ → deg-id₃
